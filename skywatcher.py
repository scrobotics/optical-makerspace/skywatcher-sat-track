import struct
from math import remainder

import serial
from loguru import logger

RA_AXIS = '1'
DEC_AXIS = '2'


def nibbles2int(n) -> int:
    n_str = n.decode()
    n_hex = bytes.fromhex(n_str)
    return struct.unpack('B', n_hex)[0]


def build_extended_command(axis, code, data):
    cmd = ':X' + axis + code + data + '\r'
    return bytearray(cmd, 'utf-8')


class SkyWatcher:
    def __init__(self, port):
        self.ser = serial.Serial(port, 115200, timeout=0.05)
        self.get_extended_version()
        self.get_version()
        self.start(RA_AXIS)
        self.start(DEC_AXIS)

        # According to SkyWatcher, sending this request forces the firmware to use 32-bit resolution.
        # Previous versions of the firmware use 24-bit.
        ra_res = self.get_resolution(RA_AXIS)
        dec_res = self.get_resolution(DEC_AXIS)
        assert ra_res == dec_res
        self.resolution = ra_res

    def __del__(self):
        # TODO wait for buffer empty or mount ready to receive orders

        self.stop(DEC_AXIS)
        self.stop(RA_AXIS)

        self.ser.close()

    def _send(self, cmd: bytearray):
        logger.debug('> ' + cmd.decode())
        self.ser.write(cmd)

    def _recv(self) -> bytearray:
        data = b''

        while True:
            c = self.ser.read()

            if len(c) == 0:  # Timeout
                break

            data += c
            if c == b'\r':
                break

        logger.debug('< ' + data.decode())
        return data

    def _request(self, cmd):
        self._send(cmd)
        return self._recv()

    def degs_to_revs(self, degs):
        degs = remainder(-degs, 360)
        return int(degs / 360 * self.resolution)

    def revs_to_degs(self, revs):
        revs = remainder(-revs, self.resolution)
        return float(revs * 360 / self.resolution)

    def get_extended_version(self):
        cmd = build_extended_command(RA_AXIS, '00', '05')
        ver = self._request(cmd)
        if len(ver) == 0:
            raise Exception('Failed to retrieve version')

        logger.info('Version: {}.{}'.format(
            nibbles2int(ver[3:5]), nibbles2int(ver[5:7])))
        logger.info('Mount: 0x{:02x}'.format(nibbles2int(ver[1:3])))

    def get_version(self):
        ver = self._request(b':e2\r')  # non-extended
        if len(ver) == 0:
            raise Exception('Failed to retrieve version')

        logger.info('Version: {}.{}'.format(
            nibbles2int(ver[1:3]), nibbles2int(ver[3:5])))
        logger.info('Mount: 0x{:02x}'.format(nibbles2int(ver[5:7])))

    def get_status(self, axis):
        cmd = build_extended_command(axis, '00', '01')
        res = self._request(cmd)
        res = res[-3:-1]
        res = res.decode()
        res = bytes.fromhex(res)
        res = struct.unpack('B', res)[0]
        if res & 0x01:
            logger.info("Slewing")
        if res & 0x04:
            logger.info("Trajectory")
        if res & 0x40:
            logger.info("Ready")

        return res

    def is_moving(self, axis):
        return self.get_status(axis) & 0x01

    def get_resolution(self, axis):
        cmd = build_extended_command(axis, '00', '02')
        res = self._request(cmd)
        res = res[1:-1]
        res = res.decode()
        res = bytes.fromhex(res)
        res = struct.unpack('>i', res)[0]
        logger.debug(f'Resolution of {axis} is {res}')
        return res

    def get_position(self, axis):
        cmd = build_extended_command(axis, '00', '03')
        res = self._request(cmd)
        res = res[1:-1]
        pos = res.decode()
        pos = bytes.fromhex(pos)
        return struct.unpack('>i', pos)[0]

    def set_position(self, axis, pos):
        pos = struct.pack('>i', self.degs_to_revs(pos))
        cmd = build_extended_command(axis, '01', pos.hex().upper())
        return self._request(cmd)

    def set_clock(self, ts):
        ts = struct.pack('>q', ts)
        cmd = build_extended_command(RA_AXIS, '0B', ts.hex().upper())
        return self._request(cmd)

    def start(self, axis):
        cmd = build_extended_command(axis, '05', '05')
        return self._request(cmd)

    def stop(self, axis):
        cmd = build_extended_command(axis, '05', '04')
        return self._request(cmd)

    def slew(self, axis, ticks=0):
        if ticks == 0:
            # Compensate the rotation of the Earth
            ticks = self.resolution / 24 / 60 / 60
        speed = struct.pack('>q', int(ticks * 1024))
        cmd = build_extended_command(axis, '02', speed.hex().upper())
        return self._request(cmd)

    def goto_and_slew(self, axis, pos, ticks=0):
        pos = struct.pack('>i', self.degs_to_revs(pos))
        ticks = struct.pack('>Q', int(ticks * 1024))
        params = pos + ticks
        cmd = build_extended_command(axis, '04', params.hex().upper())
        return self._request(cmd)

    def update_tracepoint_and_velocity(self, ts, pos, ticks=None):
        ts_bytes = struct.pack('>q', ts)
        if pos is None:
            pos_ra = struct.pack('>i', 0)
            pos_dec = struct.pack('>i', 0)
        else:
            pos_ra = struct.pack('>i', self.degs_to_revs(pos.ra))
            pos_dec = struct.pack('>i', self.degs_to_revs(pos.dec))
        params = ts_bytes + pos_ra + pos_dec
        code = '0E'

        if ticks:
            ticks_ra = self.degs_to_revs(ticks[0])
            ticks_ra = struct.pack('>i', int(ticks_ra * 1024))
            ticks_dec = self.degs_to_revs(ticks[1])
            ticks_dec = struct.pack('>i', int(ticks_dec * 1024))
            params += ticks_ra + ticks_dec
            code = '0D'

        cmd = build_extended_command(RA_AXIS, code, params.hex().upper())
        res = self._request(cmd)
        res = res[1:-1]

        buf_len = res[32:].decode()
        buf_len = bytes.fromhex(buf_len)
        buf_len = struct.unpack('>H', buf_len)
        return buf_len[0]

    def get_available_buffer(self):
        return self.update_tracepoint_and_velocity(ts=0, pos=None)
