import csv
import sys
import time
from collections import namedtuple
from datetime import datetime

import click
from loguru import logger

import skywatcher


def pos_logging(mount):
    ra_pos = mount.get_position(skywatcher.RA_AXIS)
    dec_pos = mount.get_position(skywatcher.DEC_AXIS)
    logger.debug("Current pos (RA/DEC) {:.2f}º/{:.2f}º".format(
        mount.revs_to_degs(ra_pos), mount.revs_to_degs(dec_pos)))
    logger.debug(f"Current pos ticks (RA/DEC) {ra_pos}/{dec_pos}")


CelestialPosition = namedtuple('CelestialPosition', ['ra', 'dec'])


def get_current_time():
    date = datetime.utcnow()
    us = date.timestamp() * 1e6  # seconds to microseconds
    return int(us)


def get_timestamp(timestamp):
    date = datetime.strptime(timestamp, '%Y/%m/%d %H:%M:%S.%f')
    us = date.timestamp() * 1e6  # seconds to microseconds
    return int(us)


def get_coordinates(ra, dec):
    ra = ra.strip()
    dec = dec.strip()
    return CelestialPosition(ra=float(ra), dec=float(dec))


def get_range(range):
    return float(range.strip())


@click.command()
@click.option("-p", "--port", required=True, help="Mount USB serial port (e.g. /dev/ttyUSB0)")
@click.option("-t", "--trace", required=True, type=click.Path(exists=True), help="File representing the satellite footprint trace")
@click.option("-v", "--verbose", count=True, help="Enable debug logs")
def main(port, trace, verbose):
    logger.remove()
    if verbose == 0:
        logger.add(sys.stderr, level="WARNING")
    elif verbose == 1:
        logger.add(sys.stderr, level="INFO")
    else:
        logger.add(sys.stderr, level="DEBUG")

    try:
        mount = skywatcher.SkyWatcher(port)
        mount.get_status(skywatcher.DEC_AXIS)
    except Exception as e:
        logger.info('Is mount connected?')
        raise e

    mount.set_clock(get_current_time())

    with open(trace) as f:
        data = csv.reader(f)
        header = next(data)
        row_prev = next(data)

        # TODO use astropy to get current Polaris coordinates or read the current position through drivers
        logger.info('Sync to Polaris')
        pos_tuple = CelestialPosition(37.9083, 89.26975)
        mount.set_position(skywatcher.RA_AXIS, pos_tuple.ra)
        mount.set_position(skywatcher.DEC_AXIS, pos_tuple.dec)

        pos_logging(mount)  # Debug

        logger.info('Go to initial position')
        ts_prev = get_timestamp(row_prev[0])
        coord_prev = get_coordinates(row_prev[2], row_prev[3])
        # TODO pick slew direction
        mount.goto_and_slew(skywatcher.RA_AXIS, coord_prev.ra)
        # mount.goto_and_slew(skywatcher.DEC_AXIS, coord_prev.dec)
        mount.goto_and_slew(skywatcher.RA_AXIS,
                            coord_prev.ra - 360)  # CCW initial rotation

        # Wait till goto done
        logger.info("Going to initial position...")
        while mount.is_moving(skywatcher.DEC_AXIS) or mount.is_moving(skywatcher.RA_AXIS):
            time.sleep(0.5)
            pos_logging(mount)  # Debug
        logger.info("GOTO DONE")

        # Hold on till timestamp is near
        logger.info("Waiting for sat in FOV...")
        # Compensate the rotation of the Earth
        mount.slew(skywatcher.RA_AXIS)
        mount.slew(skywatcher.DEC_AXIS)
        rem_s = ts_prev - get_current_time()
        if rem_s < 0:
            logger.exception("Satellite not in FOV anymore")
            return

        logger.info(f"{rem_s*1e-6} seconds remaining")
        while get_current_time() < (ts_prev - 120*1e3):
            time.sleep(0.1)
        logger.info("Wait DONE")

        # Start trajectory tracing
        for row in data:
            ts_prev = get_timestamp(row_prev[0])
            ts_rate = get_timestamp(row[0]) - ts_prev * 1e-6  # s
            coord = get_coordinates(row[2], row[3])
            coord_prev = get_coordinates(row_prev[2], row_prev[3])
            ra_rate = coord.ra - coord_prev.ra
            dec_rate = coord.dec - coord_prev.dec

            ticks_tuple = (ra_rate / ts_rate, dec_rate / ts_rate)
            buf_len = mount.update_tracepoint_and_velocity(
                ts_prev, coord_prev, ticks_tuple)

            while buf_len == 0:
                buf_len = mount.get_available_buffer()
                logger.warning("Buffer full")
            row_prev = row

        logger.warning("File over")


if __name__ == "__main__":
    main()
