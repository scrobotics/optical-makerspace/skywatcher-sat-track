# SkyWatcher's Satellite Tracking

## Usage
This tool needs a satellite trace file, which can be generated with [PreviSat](https://sourceforge.net/projects/previsat/) or with [our own tool (*experimental*)](https://gitlab.com/scrobotics/optical-makerspace/sat-trace-generator).

```bash
# Show help
$ python3 tracker.py --help                                              
Usage: tracker.py [OPTIONS]

Options:
  --port TEXT   Mount USB serial port (e.g. /dev/ttyUSB0)  [required]
  --trace PATH  File representing the satellite footprint trace  [required]
  -v, --verbose     Enable debug logs
  --help            Show this message and exit.

# Launch it
$ python3 tracker.py --port /dev/ttyUSB0 --trace static/20220823T0415_20220823T0420_25544.csv -vv
```

## How to use it
1. Use PreviSat to generate the tracepoints data set of a selected satellite (more details in SkyWatcher's [official guide](.static/How_to_track_satellites_with_a_Skywatcher_telscope_mount.pdf)) 
2. Set up the mount and sync to Polaris (e.g. with KStars)
3. Update the script manually*
    - Update Polaris coordinates [tracker.py](https://gitlab.com/scrobotics/optical-makerspace/skywatcher-sat-track/-/blob/master/tracker.py?ref_type=heads#L73)
    - Update the initial slew direction [tracker.py](https://gitlab.com/scrobotics/optical-makerspace/skywatcher-sat-track/-/blob/master/tracker.py?ref_type=heads#L84)
4. Launch `tracker.py` as indicated above

*Improving this step is a WIP

## License

[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://gitlab.com/scrobotics/optical-makerspace/skywatcher-sat-track/-/blob/master/COPYING)

This tool is released under the MIT license, hence allowing commercial use of the library. Please refer to the [COPYING](https://gitlab.com/scrobotics/optical-makerspace/skywatcher-sat-track/-/blob/master/COPYING) file.
